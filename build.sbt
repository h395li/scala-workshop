name := "Scala Workshop"

version := "1.0"

scalaVersion := "2.12.18"

libraryDependencies += "org.apache.spark" %% "spark-sql" % "3.5.1"
lazy val root = (project in file("."))
  .settings(
    name := "scala-proj"
  )
