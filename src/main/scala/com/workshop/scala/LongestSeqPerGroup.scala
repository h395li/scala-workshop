package com.workshop.scala

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions.{count, max, rank}

object LongestSeqPerGroup {
  val spark = SparkSession.builder.appName("scala-workshop").getOrCreate()
  import spark.implicits._


  // Finding Longest Sequence (Window Aggregation)
  def run(showRes : Boolean = false) : Unit = {
    //    Write a structured query that finds the longest sequence of consecutive numbers.
    //
    val data = Seq(
      (1,1),
      (1,2),
      (1,4),
      (1,7),
      (1,8),
      (1,9),
      (2,1),
      (3,1),
      (3,2),
      (3,3)).toDF("id", "time")

    val resdf = data.withColumn("rnk", rank().over(Window.partitionBy("id").orderBy("time"))).
      withColumn("group", $"time" - $"rnk").
      groupBy("id", "group").
      agg(count("*").as("cnt")).
      groupBy("id").
      agg(max("cnt").as("longest"))

    if(showRes){
      resdf.show
    }
  }
}
