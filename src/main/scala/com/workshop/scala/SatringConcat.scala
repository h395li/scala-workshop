package com.workshop.scala

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.concat_ws

object SatringConcat {
  val spark = SparkSession.builder.appName("scala-workshop").getOrCreate()
  import spark.implicits._


  //  Converting Arrays of Strings to String
  def run(showRes : Boolean = false) : Unit = {
    val words = Seq(Array("hello", "world")).toDF("words")
    val resdf = words.withColumn("str", concat_ws(" ", $"words"))
    if(showRes){
      resdf.show
    }
  }
}
