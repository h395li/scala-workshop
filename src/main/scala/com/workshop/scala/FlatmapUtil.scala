package com.workshop.scala

import org.apache.spark.sql.types.{ArrayType, StringType, StructType}
import org.apache.spark.sql.{Row, SparkSession}

object FlatmapUtil {
  val spark = SparkSession.builder.appName("scala-workshop").getOrCreate()

  import spark.implicits._

  //  Using Dataset.flatMap Operator
  def run(showRes: Boolean = false) : Unit = {
    /*
    Write a structured query (using spark-shell or Databricks Community Edition) that creates as many rows as the number of elements in a given array column.
    The values of the new rows should be the elements of the array column themselves.
     */
    val nums = Seq(Seq(1,2,3)).toDF("nums")

    val resdf= nums.flatMap(row => row.getSeq[Int](0).map((row.getSeq[Int](0),_))).
      toDF("nums", "num")

    if(showRes){
      resdf.show
    }
  }

  def flatmapUtil_v2(showRes: Boolean = false) : Unit = {
    val arrayStructureData = Seq(
      Row("James,,Smith",List("Java","Scala","C++"),"CA"),
      Row("Michael,Rose,",List("Spark","Java","C++"),"NJ"),
      Row("Robert,,Williams",List("CSharp","VB","R"),"NV")
    )
    val arrayStructureSchema = new StructType()
      .add("name",StringType)
      .add("languagesAtSchool", ArrayType(StringType))
      .add("currentState", StringType)

    val df = spark.createDataFrame(
      spark.sparkContext.parallelize(arrayStructureData),arrayStructureSchema)

    val resdf = df.flatMap(f=> f.getSeq[String](1).map((f.getString(0),_,f.getString(2))))
      .toDF("Name","language","State")


    if(showRes){
      resdf.show
    }
  }
}
