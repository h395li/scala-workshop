package com.workshop.scala

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.first

object FisrtNotNulCol {
  val spark = SparkSession.builder.appName("scala-workshop").getOrCreate()
  import spark.implicits._

  //   Finding First Non-Null Value per Group
  def run(showRes : Boolean = false) : Unit = {
    //    Write a structured query that finds the first non-null value per group.
    //
    val data = Seq(
      (None, 0),
      (None, 1),
      (Some(2), 0),
      (None, 1),
      (Some(4), 1)).toDF("id", "group")

    val resdf = data.groupBy("group").
      agg(first($"id", ignoreNulls = true).as("first_non_null"))
    if(showRes){
      resdf.show
    }
  }
}
