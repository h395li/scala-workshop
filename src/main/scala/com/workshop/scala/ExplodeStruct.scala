package com.workshop.scala

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{avg, col, explode}


case class MovieRatings(movieName: String, rating: Double)
case class MovieCritics(name: String, movieRatings: Seq[MovieRatings])
object ExplodeStruct extends Func {
  val spark = SparkSession.builder.appName("scala-workshop").getOrCreate()

  import spark.implicits._

  //Structs for column names and values
  def run(showRes: Boolean = false): Unit = {
    /*
    Write a structured query that “transpose” a dataset so a new dataset uses column names and values from a struct column.
     */

    val movies_critics = Seq(
      MovieCritics("Manuel", Seq(MovieRatings("Logan", 1.5), MovieRatings("Zoolander", 3), MovieRatings("John Wick", 2.5))),
      MovieCritics("John", Seq(MovieRatings("Logan", 2), MovieRatings("Zoolander", 3.5), MovieRatings("John Wick", 3))))
    val ratings = movies_critics.toDF()
    /*
    +------+--------------------------------------------------+         +------+-----+---------+---------+
    |name  |movieRatings                                      |         |name  |Logan|Zoolander|John Wick|
    +------+--------------------------------------------------+         +------+-----+---------+---------+
    |Manuel|[[Logan, 1.5], [Zoolander, 3.0], [John Wick, 2.5]]|   =>    |Manuel|1.5  |3.0      |2.5      |
    |John  |[[Logan, 2.0], [Zoolander, 3.5], [John Wick, 3.0]]|         |John  |2.0  |3.5      |3.0      |
    +------+--------------------------------------------------+         +------+-----+---------+---------+
     */


    val newdf = ratings.withColumn("exploded", explode(col("movieRatings"))).drop("movieRatings")
    //     +------+----------------+
    //     |  name|            test|
    //     +------+----------------+
    //     |Manuel|    {Logan, 1.5}|
    //     |Manuel|{Zoolander, 3.0}|
    //     |Manuel|{John Wick, 2.5}|
    //     |  John|    {Logan, 2.0}|
    //     |  John|{Zoolander, 3.5}|
    //     |  John|{John Wick, 3.0}|
    //     +------+----------------+
    val resdf = newdf.select(col("name"), col("exploded.movieName"), col("exploded.rating")).groupBy("name").pivot("movieName").agg(avg("rating"))
    if (showRes) {
      resdf.show()
    }

  }

}
