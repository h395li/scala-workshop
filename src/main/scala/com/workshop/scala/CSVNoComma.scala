package com.workshop.scala

import org.apache.spark.sql.SparkSession

object CSVNoComma {
  val spark = SparkSession.builder.appName("scala-workshop").getOrCreate()
  import spark.implicits._

  //  Why are all fields null when querying with schema
  def run(showRes : Boolean = false): Unit = {

    /*
    Write a structured query (using spark-shell or Databricks Community Edition) that loads a dataset with a proper schema with timestamp and prints out the rows to the standard output
     */
    val path = "file:////media/winston/Wisdom2/proj/scala-proj/src/main/resources/q20.csv"

    val input = spark.read.
      option("delimiter", "|").
      csv(path).drop("_c2")

    val newColumnNames = Seq("date", "ip")
    val resdf = input.toDF(newColumnNames: _*)
    if(showRes){
      resdf.show
    }
  }
}
