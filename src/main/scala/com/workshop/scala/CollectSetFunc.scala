package com.workshop.scala

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{col, collect_set, udf}

object CollectSetFunc {
  val spark = SparkSession.builder.appName("scala-workshop").getOrCreate()

  import spark.implicits._


  // Limiting collect_set Standard Function
  def run(showRes: Boolean = false): Unit = {
    /*
    Write a structured query that limits collect_set standard function.
     */

    val input = spark.range(50).withColumn("key", $"id" % 5)
    val firstThree = udf((x: List[Int]) => {
      x.take(3)
    }
    )
    val resdf = input.groupBy("key").agg(collect_set(input("id")).alias("all"))
      .withColumn("only_first_three", firstThree(col("all")))

    if (showRes) {
      resdf.show()
    }

  }
}
