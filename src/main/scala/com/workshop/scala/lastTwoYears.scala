package com.workshop.scala

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{add_months, current_date, date_format}

object lastTwoYears {
  val spark = SparkSession.builder.appName("scala-workshop").getOrCreate()

  import spark.implicits._

  //  year month of last 24 months
  def run(showRes : Boolean = false): Unit = {
    /*
    Write a structured query (using spark-shell or Databricks Community Edition) that show the years and months for the past 24 months (2 years).
     */
    val resdf = spark.range(24).
      withColumn("DATE", add_months(current_date(), $"id" * -1)).
      withColumn("YearMon", date_format($"date", "yyyyMM"))
    //    date_add and date_sub for days

    if(showRes){
      resdf.show
    }
  }
}
