package com.workshop.scala

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.max

object MaxPerGroup {
  val spark = SparkSession.builder.appName("scala-workshop").getOrCreate()
  import spark.implicits._

  //  Finding maximum values per group (groupBy)
  def run(showRes : Boolean = false) : Unit = {
    /*
    Develop a standalone Spark SQL application (using IntelliJ IDEA) that finds the highest (maximum) numbers per group.
     */
    val nums = spark.range(5).withColumn("group", 'id % 2)

    val resdf = nums.groupBy($"group").
      agg(max("id").as("max_id"))
    if(showRes){
      resdf.show
    }
  }
}
