package com.workshop.scala

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions.{col, isnull, lead, row_number, when}

object RowMerge extends Func {

  val spark = SparkSession.builder.appName("scala-workshop").getOrCreate()

  import spark.implicits._

  //Merging two rows
  def run(showRes: Boolean = false): Unit = {

    /*
    Write a structured query that “merges” two rows of the same id (to replace nulls).
     */

    val input = Seq(
      ("100","John", Some(35),None),
      ("100","John", None,Some("Georgia")),
      ("101","Mike", Some(25),None),
      ("101","Mike", None,Some("New York")),
      ("103","Mary", Some(22),None),
      ("103","Mary", None,Some("Texas")),
      ("104","Smith", Some(25),None),
      ("105","Jake", None,Some("Florida"))).toDF("id", "name", "age", "city")

    val resdf = input.
      withColumn("rowNum", row_number().over(Window.partitionBy(col("id")).orderBy(col("id")))).
      withColumn("nextRow",
        lead(col("city"), 1).over(Window.partitionBy(col("id")).orderBy(col("id")))
      ).
      withColumn("merged", when(isnull(col("city")), col("nextRow")).otherwise(col("city"))).
      filter(col("rowNum") === 1).
      select(col("id"), col("name"), col("age"), col("merged").as("city"))

    if (showRes) {
      resdf.show()
    }
  }

}
