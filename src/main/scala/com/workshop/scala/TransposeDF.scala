package com.workshop.scala

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{array, col, explode, first, lit, struct}

object TransposeDF {

  val spark = SparkSession.builder.appName("scala-workshop").getOrCreate()
  import spark.implicits._

//  Using pivot to generate a single-row matrix
  def run(showRes : Boolean = false) : Unit = {

    /*
    Write a structured query that transposes (pivots) a two-column dataframe to a single-row one (long to wide).
     */
    val input = Seq(
      (1, "MV1"),
      (2, "MV2"),
      (3, "VPV"),
      (4, "Others")).toDF("id", "value")

    val kv = explode(array(input.columns.tail.map {
      c => struct(lit(c).alias("k"), col(c).alias("v"))
    }: _*))

    val resdf = input.
      withColumn("kv", kv).
      select($"id", $"kv.k", $"kv.v").
      groupBy($"k").
      pivot("id").
      agg(first($"v")).
      orderBy($"k").
      withColumnRenamed("k", "id")

    if(showRes){
      resdf.show
    }
  }
}
