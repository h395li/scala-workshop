package com.workshop.scala

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions.sum
import org.apache.spark.sql.types.IntegerType

object RunningTotal {
  val spark = SparkSession.builder.appName("scala-workshop").getOrCreate()
  import spark.implicits._

  //  Calculating Running Total / Cumulative Sum
  def run(showRes : Boolean = false) : Unit = {
    //      Write a structured query that calculates running total (aka cumulative sum or partial sum) of items sold over time per department.
    val input = spark.read.option("header", "true").
      option("inferSchema", "true").
      csv("file:////media/winston/Wisdom2/proj/scala-proj/src/main/resources/q34.csv")

    val resdf = input.withColumn("running_total", sum($"items_sold").
      over(Window.partitionBy($"department").
        orderBy($"time")) cast IntegerType)

    if(showRes){
      resdf.show
    }
  }
}
