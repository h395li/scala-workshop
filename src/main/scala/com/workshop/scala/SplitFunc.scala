package com.workshop.scala

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{col, udf}

object SplitFunc extends Func{
  val spark = SparkSession.builder.appName("scala-workshop").getOrCreate()

  import spark.implicits._

  //  split function with variable delimiter per row
  def run(showRes: Boolean = false): Unit = {
    /*
    Write a structured query that splits a column by using delimiters from another column.

    EXTRA Write a structured query that removes empty tokens.
     */
    val input = Seq(
      ("50000.0#0#0#", "#"),
      ("0@1000.0@", "@"),
      ("1$", "$"),
      ("1000.00^Test_string", "^")).toDF("VALUES", "Delimiter")

    //+-------------------+---------+      +-------------------+---------+----------------------+
    //|             VALUES|Delimiter|      |VALUES             |Delimiter|split_values          |
    //+-------------------+---------+      +-------------------+---------+----------------------+
    //|       50000.0#0#0#|        #|      |50000.0#0#0#       |#        |[50000.0, 0, 0, ]     |
    //|          0@1000.0@|        @|      |0@1000.0@          |@        |[0, 1000.0, ]         |
    //|                 1$|        $|  ->  |1$                 |$        |[1, ]                 |
    //|1000.00^Test_string|        ^|      |1000.00^Test_string|^        |[1000.00, Test_string]|
    //+-------------------+---------+      +-------------------+---------+----------------------+

    //     if $ and ^ or other special character are escaped
    //     val exprdf = df.withColumn("split", expr("split(VALUES, Delimiter)"))


    val splitfunc = udf((value: String, delim: String) => {
      val escapedDelim = delim.flatMap(c => """\""" + c)
      value.split(escapedDelim)
    })

    val resdf = input.withColumn("split_values", splitfunc(input("VALUES"), input("Delimiter")))


    val removeEmpty = udf((list: List[String]) => {
      list.filter((str) => str.nonEmpty)
    })

    val extradf = resdf.withColumn("extra", removeEmpty(col("split_values")))
    if (showRes) {
      resdf.show()
      extradf.show()
    }
  }
}
