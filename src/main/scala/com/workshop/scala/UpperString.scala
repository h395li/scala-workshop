package com.workshop.scala

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{col, upper}

object UpperString {
  val spark = SparkSession.builder.appName("scala-workshop").getOrCreate()

  import spark.implicits._
  //  Using upper Standard Function
  def run(showRes: Boolean = false, path: String = "default", colName: String =  "city"): Unit = {
    /*
    Develop a standalone Spark SQL application (using IntelliJ IDEA) that converts one or more string columns to upper case.

    The standalone application should take at least two input parameters:

    The path of a CSV data set to load
    Column name
    The output dataset should extend the current columns with new ones with their names including the “source”, e.g. if city column were used, the output could be upper_city.
     */
    val csvPath = if (path == "default") {
      "file:////media/winston/Wisdom2/proj/scala-proj/src/main/resources/q16.csv"
    } else path

    val input = spark.read.
      option("header", "true").
      csv(csvPath)

    val colUpperName = "upper_" + colName

    val resdf = input.withColumn(colUpperName, upper(col(colName)))
    if(showRes){
      resdf.show
    }
  }

}
