package com.workshop.scala

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions.{col, explode, first, row_number}

object ArrFlatten {
  val spark = SparkSession.builder.appName("scala-workshop").getOrCreate()

  import spark.implicits._


  //  Flattening Array Columns (From Datasets of Arrays to Datasets of Array Elements)

  def run(showRes: Boolean = false) : Unit = {
    /*
    Write a structured query that loads a text file that contains the output of Dataset.show operator (aka reverse-engineer the show output).

     (intermediate) Assume that the number of elements of all arrays is the same

      (advanced) Consider pivot operator
     */

    val input = Seq(
      Seq("a","b","c"),
      Seq("X","Y","Z")).toDF

    val rnkdf = input.withColumn("val", explode(col("value"))).
      withColumn("rnk", row_number().over(Window.partitionBy("value").orderBy("value")) - 1)
    val resdf = rnkdf.groupBy("value").
      //      pivot("rnk", Seq(0, 1, 2)).
      pivot("rnk").
      agg(first("val")).drop("value")

    if(showRes){
      resdf.show
    }
  }
}
