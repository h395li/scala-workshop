package com.workshop.scala

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions.max
import org.apache.spark.sql.types.IntegerType

object GapBetweenMax {

  val spark = SparkSession.builder.appName("scala-workshop").getOrCreate()
  import spark.implicits._

  //  Calculating Gap Between Current And Highest item sold Per Department
  def run(showRes : Boolean = false) : Unit = {
    //      Write a structured query that shows the difference between the top-sold item and others per department.
    val input = spark.read.option("header", "true").
      option("inferSchema", "true").
      csv("file:////media/winston/Wisdom2/proj/scala-proj/src/main/resources/q34.csv")

    val resdf = input.withColumn("diff", (max($"items_sold").
      over(Window.partitionBy($"department")) - $"items_sold").cast(IntegerType))
    if(showRes){
      resdf.show
    }
  }
}
