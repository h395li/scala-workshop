package com.workshop.scala

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.expr

object ExplodeStructArray extends Func {
  val spark = SparkSession.builder.appName("scala-workshop").getOrCreate()

  import spark.implicits._


  // Exploding structs array
  def run(showRes: Boolean = false): Unit = {
    /*
    Write a structured query that “explodes” an array of structs (of open and close hours).
     */
    val input = spark.read.
      option("multiline", "true").
      json("file:////media/winston/Wisdom2/proj/scala-proj/src/main/resources/q7.json")

    //   +-----------+--------------+--------------------+      +-----------+--------------+---------+---------+----------+
    //   |business_id|  full_address|               hours|      |business_id|  full_address|      day|open_time|close_time|
    //   +-----------+--------------+--------------------+      +-----------+--------------+---------+---------+----------+
    //   |        abc|random_address|{{02:00, 11:00}, ...|      |        abc|random_address|   Friday|    11:00|     02:00|
    //   +-----------+--------------+--------------------+  =>    ........................
    //                                                          |        abc|random_address|Wednesday|    11:00|     02:00|
    //                                                          +-----------+--------------+---------+---------+----------+

    val fixedColumns = Seq("business_id", "full_address")
    val daydf = input.select($"business_id", $"full_address", $"hours.*")

    //    +-----------+--------------+--------------+--------------+--------------+--------------+--------------+--------------+--------------+
    //    |business_id|  full_address|        Friday|        Monday|      Saturday|        Sunday|      Thursday|       Tuesday|     Wednesday|
    //    +-----------+--------------+--------------+--------------+--------------+--------------+--------------+--------------+--------------+
    //    |        abc|random_address|{02:00, 11:00}|{02:00, 11:00}|{02:00, 11:00}|{00:00, 11:00}|{02:00, 11:00}|{02:00, 11:00}|{02:00, 11:00}|
    //    +-----------+--------------+--------------+--------------+--------------+--------------+--------------+--------------+--------------+

    val transposedCols = daydf.columns.
      filter( col => !(fixedColumns.contains(col))).
      map(c => (s"'$c', $c"))

    val expStr:String = transposedCols.mkString(s"stack(${transposedCols.size}, ", ",", ") as (Day, Hours)")

    val transposeddf =daydf.select($"business_id", $"full_address", expr(expStr))
    //    +-----------+--------------+---------+--------------+
    //    |business_id|  full_address|      Day|         Hours|
    //    +-----------+--------------+---------+--------------+
    //    |        abc|random_address|   Friday|{02:00, 11:00}|
    //.............
    //    |        abc|random_address|Wednesday|{02:00, 11:00}|
    //    +-----------+--------------+---------+--------------+

    val resdf = transposeddf.select($"business_id", $"full_address", $"day",
      $"hours.open".as("open_time"), $"hours.close".as("close_time"))

    if(showRes){
      resdf.show
    }
  }

}
