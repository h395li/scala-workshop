package com.workshop.scala

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{collect_list, explode, split}

object WordInArr {

  val spark = SparkSession.builder.appName("scala-workshop").getOrCreate()

  import spark.implicits._

  //  Finding Ids of Rows with Word in Array Column
  def run(showRes: Boolean = false) : Unit = {
    /*
    Develop a standalone Spark SQL application (using IntelliJ IDEA) that finds the ids of the rows that have values of one column in an array column.
     */
    val input = spark.read.
      option("header", true).
      csv("file:////media/winston/Wisdom2/proj/scala-proj/src/main/resources/q10.csv")
    //    +---+------------------+-----+              +-----+------------+
    //    | id|             words| word|              |    w|         ids|
    //    +---+------------------+-----+              +-----+------------+
    //    |  1|     one,two,three|  one|              | five|   [2, 4, 5]|
    //    |  2|     four,one,five|  six|    ->        |  one|[1, 2, 3, 5]|
    //    |  3|seven,nine,one,two|eight|              |seven|         [3]|
    //    |  4|    two,three,five| five|              |  six|         [5]|
    //    |  5|      six,five,one|seven|              +-----+------------+
    //    +---+------------------+-----+
    val valdf = input.withColumn("val", explode(split($"words", ","))).drop("words")
    val resdf = input.as("v1").
      join(valdf.as("v2"),$"v1.word" === $"v2.val").
      select($"v1.word", $"v2.id").groupBy($"word").agg(collect_list($"id"))
    if(showRes){
      resdf.show
    }
  }
}
