package com.workshop.scala

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{lit, when}

object PercentRank {
  val spark = SparkSession.builder.appName("scala-workshop").getOrCreate()
  import spark.implicits._

  //  Calculating percent rank
  def run(showRes : Boolean = false) : Unit = {
    //    A dataset has employees and salaries entries in no particular order. Write a structured query that adds a new column per the following requirements:
    //
    //      Top 30% gets a value “high”
    //    The next 40% gets “average”
    //    The rest gets “low”
    val nums = spark.range(10)
    val rows = nums.count()
    val resdf = nums.withColumn("percentage", $"id" / rows).
      withColumn("val", when($"percentage" < 0.3, lit("High")).
        otherwise(when($"percentage" < 0.4, lit("Average")).
          otherwise(lit("Low")))
      )
    if(showRes){
      resdf.show
    }
  }
}
