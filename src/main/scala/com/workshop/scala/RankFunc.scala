package com.workshop.scala

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions.{col, row_number}

object RankFunc extends Func {
  // Selecting the most important rows per assigned priority

  val spark = SparkSession.builder.appName("scala-workshop").getOrCreate()

  import spark.implicits._

  def run(showRes: Boolean = false) = {
    /*
    Write a structured query that selects the most important rows per assigned priority.
     */

    val input = Seq(
      (1, "MV1"),
      (1, "MV2"),
      (2, "VPV"),
      (2, "Others")).toDF("id", "value")

    val resdf = input.withColumn("rnk", row_number().over(Window.partitionBy(col("id")).orderBy("value"))).filter("rnk = 1").drop("rnk")
    if (showRes) {
      resdf.show()
    }
  }
}
