package com.workshop.scala

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{count, lit, max, when}

object MostCommonPrefix {
  val spark = SparkSession.builder.appName("scala-workshop").getOrCreate()
  import spark.implicits._


  // Finding Most Common Non-null Prefix per Group (Occurences)
  def run(showRes : Boolean = false) : Unit = {
    //    Write a structured query that finds the most common not-null PREFIX (occurences) per UNIQUE_GUEST_ID.
    //
    val input = Seq(
      (1, "Mr"),
      (1, "Mme"),
      (1, "Mr"),
      (1, null),
      (1, null),
      (1, null),
      (2, "Mr"),
      (3, null)).toDF("UNIQUE_GUEST_ID", "PREFIX")

    val cntdf = input.groupBy("UNIQUE_GUEST_ID", "PREFIX").
      agg(count("*").as("cnt"))
    val rnkdf = cntdf.
      withColumn("nonNullCnt", when($"PREFIX".isNull, lit(0)).
        otherwise($"cnt")).
      groupBy("UNIQUE_GUEST_ID").
      agg(max("nonNullCnt").as("max"))

    val resdf = rnkdf.join(cntdf,
        cntdf("cnt") === rnkdf("max") && cntdf("UNIQUE_GUEST_ID") === rnkdf("UNIQUE_GUEST_ID"), "left").
      select(rnkdf("UNIQUE_GUEST_ID"), $"prefix").orderBy("UNIQUE_GUEST_ID")


    if(showRes){
      resdf.show
    }
  }
}
