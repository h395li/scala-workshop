package com.workshop.scala

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions.{coalesce, lag, lit, sum}
import org.apache.spark.sql.types.IntegerType

object DiffPerWindow {
  val spark = SparkSession.builder.appName("scala-workshop").getOrCreate()
  import spark.implicits._
  //  Calculating Difference Between Consecutive Rows Per Window
  def run(showRes : Boolean = false) : Unit = {
    //      Write a structured query that calculates the difference between consecutive running_total rows over time per department.
    val input = spark.read.option("header", "true").
      option("inferSchema", "true").
      csv("file:////media/winston/Wisdom2/proj/scala-proj/src/main/resources/q34.csv").
      withColumn("running_total", sum($"items_sold").
        over(Window.partitionBy($"department").
          orderBy($"time")) cast IntegerType)

    val resdf = input.withColumn("lastRow", lag("running_total", 1).
        over(Window.partitionBy($"department").
          orderBy($"time"))).
      withColumn("diff", $"running_total" - coalesce($"lastRow", lit(0))).
      drop("lastRow")

    if(showRes){
      resdf.show
    }
  }
}
