package com.workshop.scala

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{current_date, datediff, to_date}

object DateDiff {
  val spark = SparkSession.builder.appName("scala-workshop").getOrCreate()

  import spark.implicits._

  //  Difference in Days Between Dates As Strings
  def run(showRes : Boolean = false): Unit = {
    /*
    Write a structured query (using spark-shell or Databricks Community Edition) that calculates the number of days between dates given as text (in some format) and the current date.
     */

    val dates = Seq(
      "08/11/2015",
      "09/11/2015",
      "09/12/2015").toDF("date_string")

    val resdf = dates.withColumn("to_date", to_date($"date_String", "mm/dd/yyyy")).
      withColumn("diff", datediff(current_date(), $"to_date"))

    if(showRes){
      resdf.show
    }
  }

}
