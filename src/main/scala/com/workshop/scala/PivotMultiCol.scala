package com.workshop.scala

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.first

object PivotMultiCol {
  val spark = SparkSession.builder.appName("scala-workshop").getOrCreate()
  import spark.implicits._
  //  Pivoting on Multiple Columns
  def run(showRes : Boolean = false) : Unit = {
    /*
    Write a structured query that pivots a dataset on multiple columns. Since pivot aggregation allows for a single column only, find a solution to pivot on two or more columns.
     */
    val data = Seq(
      (100,1,23,10),
      (100,2,45,11),
      (100,3,67,12),
      (100,4,78,13),
      (101,1,23,10),
      (101,2,45,13),
      (101,3,67,14),
      (101,4,78,15),
      (102,1,23,10),
      (102,2,45,11),
      (102,3,67,16),
      (102,4,78,18)).toDF("id", "day", "price", "units")
    val resdf = data.groupBy("id").
      pivot($"day").
      agg(first($"price").alias("price"), first($"units").alias("ship")).
      orderBy($"id")

    if(showRes){
      resdf.show
    }
  }
}
