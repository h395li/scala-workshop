package com.workshop.scala

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{avg, collect_list}

object AvgAndCollect {
  val spark = SparkSession.builder.appName("scala-workshop").getOrCreate()
  import spark.implicits._


  //  Using pivot for Cost Average and Collecting Values
  def run(showRes : Boolean = false) : Unit = {
    /*

Write a structured query (using spark-shell or Databricks Community Edition) that calculates cost average (part 1) and collects all the values in a column (part 2).
     */

    val data = Seq(
      (0, "A", 223, "201603", "PORT"),
      (0, "A", 22, "201602", "PORT"),
      (0, "A", 422, "201601", "DOCK"),
      (1, "B", 3213, "201602", "DOCK"),
      (1, "B", 3213, "201601", "PORT"),
      (2, "C", 2321, "201601", "DOCK")
    ).toDF("id","type", "cost", "date", "ship")


    val resdf = data.groupBy($"id", $"type").
      pivot("date").
      agg(avg($"cost")).
      orderBy($"id")
    val resdf2 = data.groupBy($"id", $"type").
      pivot("date").
      agg(collect_list($"ship")).
      orderBy($"id")

    if(showRes){
      resdf.show
    }
  }

}
