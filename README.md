# Scala Workshop



## What's it about

A scala project using Spark SQL library to to solve exercises modelled real world problems and to transform input data into desired format and dimensions

List of exercises are found from https://jaceklaskowski.github.io/spark-workshop/exercises/ with tweaks of some of its input data
